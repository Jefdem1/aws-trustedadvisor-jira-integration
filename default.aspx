<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="appJeffD.AWS.TrustedAdvisor._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:DropDownList ID="ddCheckId" runat="server" AutoPostBack="True" Width="400px" OnSelectedIndexChanged="ddCheckId_SelectedIndexChanged"></asp:DropDownList>
        <br />
        <asp:DropDownList ID="ddItems" runat="server" Width="400px">
        </asp:DropDownList>
        <table>
            <tr>
                <td>Project Name:</td><td><asp:TextBox ID="txtProjectName" runat="server" Width="400px">JEF</asp:TextBox></td>
                </tr>
            <tr>
                <td>Username:</td><td><asp:TextBox ID="txtUserName" runat="server" Width="400px">jefdem</asp:TextBox></td>
                </tr>
            <tr>
                <td>Password:</td><td><asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="400px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>URL:</td><td><asp:TextBox ID="txtURL" runat="server" Width="400px">http://35.166.11.168:8080/rest/api/2/</asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="2"><asp:Button ID="btnCreateJira" runat="server" Text="Create Jira" OnClick="btnCreateJira_Click" Width="100%" />
        </td>
            </tr>
            <tr>
                <td colspan="2"><asp:Button ID="btnListJira" runat="server" Text="List Jira Issues" Width="100%" OnClick="btnListJira_Click" />
        </td>
            </tr>
        </table>
        
        
        
    </div>
       
            
    </form>
</body>
</html>
