using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace appJeffD.AWS.TrustedAdvisor
{
    public partial class _default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            fnGetChecks();
        }
        void fnGetChecks()
        {
            if (ddCheckId.Items.Count == 0)
            {
                var client = new Amazon.AWSSupport.AmazonAWSSupportClient(Amazon.RegionEndpoint.USEast1);
                var dtacreq = new Amazon.AWSSupport.Model.DescribeTrustedAdvisorChecksRequest();
                dtacreq.Language = "en";
                var dtacresp = new Amazon.AWSSupport.Model.DescribeTrustedAdvisorChecksResponse();
                dtacresp = client.DescribeTrustedAdvisorChecks(dtacreq);
                foreach (var item in dtacresp.Checks)
                {
                    ddCheckId.Items.Add(new ListItem(item.Name, item.Id));
                }
                fnGetItems();
            }
        }
        protected void ddCheckId_SelectedIndexChanged(object sender, EventArgs e)
        {
            fnGetItems();
        }
        void fnGetItems()
        {
            ddItems.Items.Clear();
            var client = new Amazon.AWSSupport.AmazonAWSSupportClient(Amazon.RegionEndpoint.USEast1);
            var dtacrreq = new Amazon.AWSSupport.Model.DescribeTrustedAdvisorCheckResultRequest();
            var dtacresp = new Amazon.AWSSupport.Model.DescribeTrustedAdvisorCheckResultResponse();
            dtacrreq.CheckId = ddCheckId.SelectedItem.Value;
            dtacresp = client.DescribeTrustedAdvisorCheckResult(dtacrreq);
            foreach (var item in dtacresp.Result.FlaggedResources)
            {
                ddItems.Items.Add(new ListItem(item.Status + ", " + item.Metadata[1] + ", " + item.Metadata[2] + ", " + item.Metadata[3] + ", " + item.Metadata[4], item.Metadata[0] + " - " + item.Metadata[1]));
            }
        }
        protected void btnCreateJira_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text != "")
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(txtURL.Text + "issue/");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                String username = txtUserName.Text;
                String password = txtPassword.Text;
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string strData = "{\"fields\": { \"project\": {\"key\": \"" + txtProjectName.Text + "\"}, \"summary\": \"" + ddItems.SelectedItem.Text + "\", \"description\": \"" + ddItems.SelectedItem.Text + "\", \"issuetype\": {\"name\": \"Task\"}}}";
                    streamWriter.Write(strData);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Response.Write(result);
                }
            }
        }
        protected void btnListJira_Click(object sender, EventArgs e)
        {
            if (txtPassword.Text != "")
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(txtURL.Text + "search?jql=project=JEF+order+by+duedate&fields=key,summary");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "GET";
                String username = txtUserName.Text;
                String password = txtPassword.Text;
                String encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                httpWebRequest.Headers.Add("Authorization", "Basic " + encoded);
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    Response.Write(result);
                }
            }
        }
    }
}
